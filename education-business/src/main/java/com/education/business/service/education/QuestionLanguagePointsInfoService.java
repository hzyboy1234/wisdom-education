package com.education.business.service.education;

import com.education.business.mapper.education.QuestionLanguagePointsInfoMapper;
import com.education.business.service.BaseService;
import com.education.model.entity.QuestionLanguagePointsInfo;
import org.springframework.stereotype.Service;

/**
 * @author zengjintao
 * @version 1.0
 * @create_at 2020/11/19 15:24
 */
@Service
public class QuestionLanguagePointsInfoService extends BaseService<QuestionLanguagePointsInfoMapper, QuestionLanguagePointsInfo> {
}
